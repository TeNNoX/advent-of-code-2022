use std::{
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Debug, Clone, Copy, Ord, PartialOrd, Eq, PartialEq)]
struct Elf {
    calories: u32,
}

fn main() {
    println!("Hello, AdventOfCode 1!");

    let f = File::open("./input").expect("Unable to open ./input");
    let f = BufReader::new(f);

    let mut elves: Vec<Elf> = vec![];

    let mut currenf_elf: Elf = Elf { calories: 0 };
    let mut max_elves = vec![currenf_elf.clone(); 3];

    let check_max = |max_elves: &mut Vec<Elf>, currenf_elf: &Elf| {
        if &max_elves[0] < currenf_elf {
            println!(
                "NEW MAX: {:?} > {:?}",
                currenf_elf.calories,
                max_elves.iter().map(|e| e.calories)
            );
            max_elves.push(currenf_elf.clone());
            max_elves.sort_unstable();
            max_elves.remove(0);
            println!("After sort: {:?}", max_elves);
        }
    };

    for line in f.lines() {
        let line = line.expect("Unable to read line");
        // println!("Line: {}", line);
        match line.as_ref() {
            "" => {
                println!("Finished elf: {:?}", currenf_elf);
                check_max(&mut max_elves, &currenf_elf);
                elves.push(currenf_elf);
                currenf_elf = Elf { calories: 0 };
            }
            &_ => {
                currenf_elf.calories += line
                    .parse::<u32>()
                    .expect(format!("Unable to parse {}", line).as_str());
            }
        }
    }
    println!("Finished elf: {:?}", currenf_elf);
    check_max(&mut max_elves, &currenf_elf);
    elves.push(currenf_elf);

    println!(
        "MAX: {:?} => {}",
        max_elves,
        max_elves.iter().map(|e| e.calories).sum::<u32>()
    );
}
