use core::panic;
use std::{
    fs,
    ops::{RangeBounds, RangeInclusive},
};

// PUZZLE: https://web.archive.org/web/20230223113025/https://adventofcode.com/2022/day/4

pub fn main() {
    println!("Hello, AdventOfCode - day 4!");
    let content = fs::read_to_string("./input").expect("Read ./input");

    println!("PART 1 - RESULT: {}", process_part1(content.clone()));
    println!();
    println!("PART 2 - RESULT: {}", process_part2(content));
}

fn process_part1(content: String) -> String {
    let mut containment_count = 0;
    for line in content.lines() {
        let line_split = line.split(",").collect::<Vec<_>>();
        let [first_str, second_str] = line_split.as_slice() else { panic!("Invalid line: {}", line); };
        let [range1, range2] = [*first_str, *second_str].map(parse_range);
        let is_containment = check_contains_range(&range1, &range2);
        println!("{:?} | {:?} => {}", range1, range2, is_containment);
        if is_containment {
            containment_count += 1;
        }
    }
    containment_count.to_string()
}

fn process_part2(content: String) -> String {
    let mut overlap_count = 0;
    for line in content.lines() {
        let line_split = line.split(",").collect::<Vec<_>>();
        let [first_str, second_str] = line_split.as_slice() else { panic!("Invalid line: {}", line); };
        let [range1, range2] = [*first_str, *second_str].map(parse_range);
        let is_overlap = check_range_overlap(&range1, &range2);
        println!("{:?} | {:?} => {}", range1, range2, is_overlap);
        if is_overlap {
            overlap_count += 1;
        }
    }
    overlap_count.to_string()
}

fn parse_range(range_str: &str) -> RangeInclusive<u16> {
    let range_split = range_str.split("-").collect::<Vec<_>>();
    let [first_str, second_str] = range_split.as_slice() else { panic!("Invalid range: {}", range_str); };
    let [start, end] = [first_str, second_str].map(|r| {
        r.parse()
            .expect(&format!("parse range part: {} of {}", r, range_str))
    });
    start..=end
}

fn check_contains_range<I>(range1: &RangeInclusive<I>, range2: &RangeInclusive<I>) -> bool
where
    I: PartialOrd<I>,
{
    (range1.contains(&range2.start()) && range1.contains(&range2.end()))
        || (range2.contains(&range1.start()) && range2.contains(&range1.end()))
}

fn check_range_overlap<I>(range1: &RangeInclusive<I>, range2: &RangeInclusive<I>) -> bool
where
    I: PartialOrd<I>,
{
    range1.start() <= range2.end() && range1.end() >= range2.start()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";

    #[test]
    fn range_check() {
        assert_eq!((1..=3).contains(&3), true); // sanity check

        assert_eq!(check_contains_range(&(1..=3), &(5..=6)), false);
        assert_eq!(check_contains_range(&(1..=3), &(2..=6)), false);
        assert_eq!(check_contains_range(&(1..=3), &(0..=2)), false);
        assert_eq!(check_contains_range(&(1..=3), &(1..=1)), true);
        assert_eq!(check_contains_range(&(1..=3), &(1..=3)), true);
        assert_eq!(check_contains_range(&(1..=3), &(2..=2)), true);
        assert_eq!(check_contains_range(&(1..=3), &(3..=3)), true);
    }

    #[test]
    fn example_part1() {
        let result = process_part1(EXAMPLE.into());
        assert_eq!(result, "2");
    }

    #[test]
    fn real_part1() {
        let result = process_part1(fs::read_to_string("./input").expect("Read ./input"));
        assert_eq!(result, "513");
    }

    #[test]
    fn example_part2() {
        let result = process_part2(EXAMPLE.into());
        assert_eq!(result, "4");
    }

    #[test]
    fn real_part2() {
        let result = process_part2(fs::read_to_string("./input").expect("Read ./input"));
        assert_eq!(result, "TODO");
    }
}
