use std::fs;

// PUZZLE: https://web.archive.org/web/20230224131121/https://adventofcode.com/2022/day/6

pub fn main() {
    println!("Hello, AdventOfCode - day 6!");
    let content = fs::read_to_string("./input").expect("Read ./input");

    println!("PART 1 - RESULT: {}", process_part1(content.clone()));
    println!();
    println!("PART 2 - RESULT: {}", process_part2(content));
}

fn process_part1(content: String) -> String {
    find_marker(content, 4)
}

fn find_marker(content: String, window_size: usize) -> String {
    for (pos, window) in content
        .chars()
        .collect::<Vec<_>>()
        .windows(window_size)
        .enumerate()
    {
        let mut chars = window.iter().collect::<Vec<&char>>();
        chars.sort();
        chars.dedup();
        // dbg!(&chars);
        let is_marker = chars.len() == window_size;
        println!(
            "[{}: {}] marker ? {}",
            pos,
            window.iter().collect::<String>(),
            is_marker
        );
        if is_marker {
            return (pos + window_size).to_string();
        }
    }
    panic!("No marker found");
}

fn process_part2(content: String) -> String {
    find_marker(content, 14)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example_part1() {
        assert_eq!(process_part1("mjqjpqmgbljsphdztnvjfqwrcgsmlb".into()), "7");
        assert_eq!(process_part1("bvwbjplbgvbhsrlpgdmjqwftvncz".into()), "5");
        assert_eq!(process_part1("nppdvjthqldpwncqszvftbrmjlhg".into()), "6");
        assert_eq!(
            process_part1("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg".into()),
            "10"
        );
        assert_eq!(
            process_part1("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw".into()),
            "11"
        );
    }

    #[test]
    fn real_part1() {
        let result = process_part1(fs::read_to_string("./input").expect("Read ./input"));
        assert_eq!(result, "1238");
    }

    #[test]
    fn example_part2() {
        assert_eq!(process_part2("mjqjpqmgbljsphdztnvjfqwrcgsmlb".into()), "19");
        assert_eq!(process_part2("bvwbjplbgvbhsrlpgdmjqwftvncz".into()), "23");
        assert_eq!(process_part2("nppdvjthqldpwncqszvftbrmjlhg".into()), "23");
        assert_eq!(
            process_part2("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg".into()),
            "29"
        );
        assert_eq!(
            process_part2("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw".into()),
            "26"
        );
    }

    #[test]
    fn real_part2() {
        let result = process_part2(fs::read_to_string("./input").expect("Read ./input"));
        assert_eq!(result, "SOLUTION");
    }
}
