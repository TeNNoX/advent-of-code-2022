# My solutions for AdventOfCode 2022 (in Rust)

I started learning rust a few months ago and felt this was a good chance to improve upon and generalize my skills, as well as compare (not compete) with [other people's strategies](https://www.youtube.com/watch?v=bkvSRfgDG-E&t=130s)).

I also tried to **focus on easy-to-understand and easy-to-maintain style** of code as opposed to efficient or [short](https://www.reddit.com/r/adventofcode/comments/zac2v2/comment/iylda9n/?utm_source=share&utm_medium=web2x&context=3).

### Dev setup
1. Install dependencies:  
    (Option A) `direnv allow` if you have direnv & nix  
    (Option B) Install rust, cargo & `cargo-watch` on your own
2. Go to `dayXX` subdir
3. Run
    - I developed using `cargo watch -cx 'test'`
    - Can also be run using just `cargo r`