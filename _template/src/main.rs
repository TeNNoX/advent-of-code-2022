use std::fs;

// PUZZLE: https://web.archive.org/web/*/https://adventofcode.com/2022/day/XX

pub fn main() {
    println!("Hello, AdventOfCode - day XX!");
    let content = fs::read_to_string("./input").expect("Read ./input");

    println!("PART 1 - RESULT: {}", process_part1(content.clone()));
    // println!();
    // println!("PART 2 - RESULT: {}", process_part2(content));
}

fn process_part1(content: String) -> String {
    todo!()
}

fn process_part2(content: String) -> String {
    todo!()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "";

    #[test]
    fn example_part1() {
        let result = process_part1(EXAMPLE.into());
        assert_eq!(result, "TODO");
    }

    // #[test]
    // fn real_part1() {
    //     let result = process_part1(fs::read_to_string("./input").expect("Read ./input"));
    //     assert_eq!(result, "SOLUTION");
    // }

    // #[test]
    // fn example_part2() {
    //     let result = process_part2(EXAMPLE.into());
    //     assert_eq!(result, "TODO");
    // }

    // #[test]
    // fn real_part2() {
    //     let result = process_part2(fs::read_to_string("./input").expect("Read ./input"));
    //     assert_eq!(result, "SOLUTION");
    // }
}
