#![feature(iter_intersperse, once_cell)]
#![allow(unused_imports)]
use anyhow::Context;
use clap::{arg, command};
use colored::{ColoredString, Colorize};
use crossterm::{
    cursor::{Hide, MoveTo},
    execute,
    terminal::{Clear, ClearType},
};
use dbg_pls::{color, DebugPls};
use ndarray::{s, Array2, ArrayBase, Dim, ViewRepr};
use nom::{
    bytes::complete::take_while1,
    character::{
        complete::{digit1, newline},
        is_digit,
    },
    combinator::{map_res, opt},
    multi::{many1, separated_list1},
    IResult, Parser,
};
use once_cell::sync::OnceCell;
use rayon::prelude::*;
use std::{
    collections::{BTreeMap, HashMap},
    fmt::{Display, Formatter},
    fs,
    io::{stdout, Write},
    iter::Map,
    sync::atomic::{AtomicUsize, Ordering},
    thread::sleep,
    time::Duration,
};
use strum::{EnumIter, IntoEnumIterator};

// PUZZLE: https://web.archive.org/web/20230224131121/https://adventofcode.com/2022/day/8

/// Advent of Code Day 8
#[derive(clap::Parser, Debug)]
#[command(author="tennox", version, about, long_about = None)]
struct Args {
    /// Input file (default: ./input)
    input_file: Option<String>,

    /// Flag to enable fast mode (parallel & no debug prints)
    #[arg(long)]
    fast: bool,
    /// Flag to enable fancy mode (slow, interactive, with screen clearing)
    #[arg(long)]
    fancy: bool,
}

#[derive(Debug)]
struct Grid(Array2<u8>);
#[derive(Debug, DebugPls)]
struct Pos {
    row: usize,
    col: usize,
}

#[derive(Debug, DebugPls, EnumIter, PartialEq, Hash, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub fn main() {
    let args = <Args as clap::Parser>::parse();

    println!("Hello, AdventOfCode - day 8!");
    let content = fs::read_to_string(args.input_file.as_ref().unwrap_or(&"./input".into()))
        .expect("Read ./input");

    let result_p1 = process_part1(content.clone(), &args);
    println!("PART 1 - RESULT: {}", result_p1);
    if !args.fast {
        sleep(Duration::from_secs(5));
        println!("");
    }

    let result_p2 = process_part2(content, &args);

    if !args.fast {
        println!("PART 1 - RESULT: {}", result_p1); // print again, as there was a lot of output above
    }
    println!("PART 2 - RESULT: {}", result_p2);
}

fn process_part1(content: String, args: &Args) -> String {
    let (_, grid) = parse_grid(content.as_str()).expect("parse grid");

    let visible_count = AtomicUsize::new(0);
    maybe_par_iter(
        grid.iter_with_pos().collect::<Vec<_>>(),
        args.fast,
        |(row, col, _)| {
            if args.fancy {
                execute!(stdout(), Clear(ClearType::All)).expect("clear terminal");
                execute!(stdout(), MoveTo(0, 0)).expect("move cursor");
            }
            if !args.fast {
                println!("Checking {:?}", [row, col]);
            }
            let visible_directions: Vec<Direction> = Direction::iter()
                .filter(|dir| grid.can_see(&Pos { row, col }, dir, !args.fast))
                .collect();
            if !visible_directions.is_empty() {
                visible_count.fetch_add(1, Ordering::Relaxed);
            }

            if !args.fast {
                color!(&visible_directions);
                println!("{}", grid.fmt_focus(Pos { row, col }, &visible_directions));
                println!("");
                stdout().flush().expect("flush");
                if args.fancy {
                    sleep(Duration::from_millis(100));
                }
            }
        },
    );

    visible_count.load(Ordering::Relaxed).to_string()
}

fn process_part2(content: String, args: &Args) -> String {
    let (_, grid) = parse_grid(content.as_str()).expect("parse grid");

    let max_scenic_score = AtomicUsize::new(0);
    maybe_par_iter(
        grid.iter_with_pos().collect::<Vec<_>>(),
        args.fast,
        |(row, col, _)| {
            if args.fancy {
                execute!(stdout(), Clear(ClearType::All)).expect("clear terminal");
                execute!(stdout(), MoveTo(0, 0)).expect("move cursor");
            }
            if !args.fast {
                println!("Checking {:?}", [row, col]);
            }
            let visible_count_per_direction: HashMap<Direction, usize> = Direction::iter()
                .map(|dir| {
                    let visible_count = grid.visible_count(&Pos { row, col }, &dir, !args.fast);
                    (dir, visible_count)
                })
                .collect();
            let scenic_score = visible_count_per_direction
                .values()
                .fold(1, |acc, x| acc * x);
            max_scenic_score.fetch_max(scenic_score, Ordering::Relaxed);

            if !args.fast {
                dbg!(&visible_count_per_direction);
                color!(scenic_score);
                println!(
                    "{}",
                    grid.fmt_focus_p2(Pos { row, col }, &visible_count_per_direction)
                );
                println!("");
                stdout().flush().expect("flush stdout");
                if args.fancy {
                    sleep(Duration::from_millis(300));
                }
            }
        },
    );

    max_scenic_score.load(Ordering::Relaxed).to_string()
}

fn maybe_par_iter<I, F>(vec: Vec<I>, parallel: bool, func: F)
where
    I: Send,
    F: Fn(I) -> () + Send + Sync,
{
    if parallel {
        vec.into_par_iter().for_each(func)
    } else {
        vec.into_iter().for_each(func)
    }
}

fn parse_grid(input: &str) -> IResult<&str, Grid> {
    let (input, rows) = separated_list1(
        newline,
        digit1.map(|row_str: &str| {
            row_str
                .chars()
                .map(|c| c.to_digit(10).expect("parse char") as u8)
                .collect::<Vec<_>>()
        }),
    )(input)?;
    let shape = (rows.len(), rows[0].len());
    println!("Grid shape: {} x {}", shape.0, shape.1);
    // color!(&rows, shape);
    let grid = Grid(
        Array2::from_shape_vec(
            shape,
            rows.into_iter().flat_map(|row| row).collect::<Vec<_>>(),
        )
        .expect("rows vec in shape"),
    );

    // dbg!(&grid);
    // dbg!(&grid.0.row(0));
    // dbg!(&grid.0.column(0));
    let (input, _) = opt(newline)(input)?;
    assert!(input.is_empty());
    Ok((input, grid))
}

impl Grid {
    fn iter_with_pos(&self) -> impl Iterator<Item = (usize, usize, u8)> + '_ {
        self.0
            .rows()
            .into_iter()
            .enumerate()
            .flat_map(|(row_idx, row)| {
                row.into_iter().enumerate().map(move |(col_idx, tree)| {
                    (row_idx, col_idx, tree.clone()) as (usize, usize, u8)
                })
            })
    }

    fn fmt_focus(&self, focus: Pos, visible_from: &Vec<Direction>) -> String {
        self.0
            .rows()
            .into_iter()
            .enumerate()
            .map(|(row, row_vec)| {
                row_vec
                    .iter()
                    .enumerate()
                    .map(|(col, tree)| {
                        let mut s = tree.to_string().normal();
                        if col == focus.col as usize || row == focus.row as usize {
                            s = s.bold()
                        } else {
                            s = s.dimmed()
                        }
                        fn colorize(mut s: ColoredString, flag: bool) -> ColoredString {
                            if flag {
                                s = s.on_bright_green()
                            } else {
                                s = s.on_bright_red()
                            }
                            s
                        }
                        if col == focus.col && row < focus.row {
                            s = colorize(s, visible_from.contains(&Direction::Up));
                        } else if col == focus.col && row > focus.row {
                            s = colorize(s, visible_from.contains(&Direction::Down));
                        } else if row == focus.row && col < focus.col {
                            s = colorize(s, visible_from.contains(&Direction::Left));
                        } else if row == focus.row && col > focus.col {
                            s = colorize(s, visible_from.contains(&Direction::Right));
                        }
                        s.to_string()
                    })
                    .collect::<Vec<_>>()
                    .concat()
            })
            .collect::<Vec<_>>()
            .join("\n")
    }
    fn fmt_focus_p2(&self, focus: Pos, visible_counts: &HashMap<Direction, usize>) -> String {
        self.0
            .rows()
            .into_iter()
            .enumerate()
            .map(|(row, row_arr)| {
                row_arr
                    .iter()
                    .enumerate()
                    .map(|(col, tree)| {
                        let mut s = tree.to_string().normal();
                        if col == focus.col as usize || row == focus.row as usize {
                            s = s.bold()
                        } else {
                            s = s.dimmed()
                        }
                        fn colorize(mut s: ColoredString, flag: bool) -> ColoredString {
                            if flag {
                                s = s.on_bright_green()
                            } else {
                                s = s.on_bright_red()
                            }
                            s
                        }
                        if col == focus.col && row < focus.row {
                            s = colorize(
                                s,
                                focus.row.abs_diff(row)
                                    <= *visible_counts.get(&Direction::Up).unwrap(),
                            );
                        } else if col == focus.col && row > focus.row {
                            s = colorize(
                                s,
                                focus.row.abs_diff(row)
                                    <= *visible_counts.get(&Direction::Down).unwrap(),
                            );
                        } else if row == focus.row && col < focus.col {
                            s = colorize(
                                s,
                                focus.col.abs_diff(col)
                                    <= *visible_counts.get(&Direction::Left).unwrap(),
                            );
                        } else if row == focus.row && col > focus.col {
                            s = colorize(
                                s,
                                focus.col.abs_diff(col)
                                    <= *visible_counts.get(&Direction::Right).unwrap(),
                            );
                        }
                        s.to_string()
                    })
                    .collect::<Vec<_>>()
                    .concat()
            })
            .collect::<Vec<_>>()
            .join("\n")
    }

    pub(crate) fn trees_in_dir(
        &self,
        pos: &Pos,
        dir: &Direction,
    ) -> ArrayBase<ViewRepr<&u8>, Dim<[usize; 1]>> {
        self.0.slice(match dir {
            Direction::Up => s![..pos.row;-1, pos.col],
            Direction::Down => s![pos.row + 1.., pos.col],
            Direction::Left => s![pos.row, ..pos.col;-1],
            Direction::Right => s![pos.row, pos.col + 1..],
        })
    }

    pub(crate) fn can_see(&self, pos: &Pos, dir: &Direction, verbose: bool) -> bool {
        let house_height = self.0.get(pos.index()).expect("grid has given pos");
        let trees = self.trees_in_dir(&pos, dir);
        let can_see = trees.iter().all(|height| height < house_height);
        if verbose {
            println!(
                "visible? {:?} {} > {} => {}",
                color(dir),
                trees,
                color(house_height),
                color(&can_see)
            );
        }
        can_see
    }

    pub(crate) fn visible_count(&self, pos: &Pos, dir: &Direction, verbose: bool) -> usize {
        let house_height = self.0.get(pos.index()).expect("grid has given pos");
        let trees = self.trees_in_dir(pos, dir);
        let visible_count = trees
            .iter()
            .enumerate()
            .find(|(_, &height)| &height >= house_height) // find first that blocks sight
            .map(
                |(index, _)| index + 1, /* the tree that's too high still counts */
            )
            .unwrap_or_else(|| trees.len());

        if verbose {
            println!(
                "visible? {:5?} {} > {} => {}",
                // color(&pos),
                color(dir),
                trees,
                color(house_height),
                color(&visible_count)
            );
        }
        visible_count
    }
}
impl Pos {
    fn index(&self) -> [usize; 2] {
        [self.row, self.col]
    }
}
impl Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in self
            .0
            .rows()
            .into_iter()
            .map(|row| row.iter().map(u8::to_string).collect::<String>())
        // .intersperse("\n").collect::<String>()
        {
            writeln!(f, "{}", row)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "30373
25512
65332
33549
35390";

    #[test]
    // #[ignore = "I'm on part 2 now"]
    fn example_part1() {
        let result = process_part1(
            EXAMPLE.into(),
            &Args {
                input_file: None,
                fancy: false,
                fast: false,
            },
        );
        assert_eq!(result, "21");
    }

    #[test]
    // #[ignore = "takes long"]
    fn real_part1() {
        let result = process_part1(
            fs::read_to_string("./input").expect("Read ./input"),
            &Args {
                input_file: None,
                fancy: false,
                fast: true,
            },
        );
        assert_eq!(result, "1713");
    }

    #[test]
    fn example_part2() {
        let result = process_part2(
            EXAMPLE.into(),
            &Args {
                input_file: None,
                fancy: false,
                fast: false,
            },
        );
        assert_eq!(result, "8");
    }

    #[test]
    // #[ignore = "takes long"]
    fn real_part2() {
        let result = process_part2(
            fs::read_to_string("./input").expect("Read ./input"),
            &Args {
                input_file: None,
                fancy: false,
                fast: true,
            },
        );
        assert_eq!(result, "268464");
    }
}
