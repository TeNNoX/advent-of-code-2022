# Advent of Code - day 8

This one has a few cli flags for fancy / fast mode:

### Normal mode
Prints stats and grid visualizations. You can scroll through it to inspect each step (as opposed to fancy mode)
```
cargo run --release
```

### Fancy mode
Like normal mode but clears the screen and waits after each step, enabling a nice visualization:
```
cargo run --release -- --fancy ./small-input
```
![screencast](./Screencast%20fancy%20mode.webm)

### Fast mode
This skips most output and paralellizes execution, enabling the full **solution of the puzzle in 50ms** (on my machine) - and a huge variant (`./big-input` - 10 million trees) in 10s:
```
cargo run --release -- --fast ./big-input
```

### Test mode

Tests the code with the official solution I submitted. Very useful during development.

```
cargo test
```
or if you want to see output
```
cargo test -- --nocapture
```